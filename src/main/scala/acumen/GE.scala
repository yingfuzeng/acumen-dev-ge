package acumen
import Array._
import util.Names.name
import Pretty._
import acumen.interpreters.Common._
import Simplifier._
import Error._
/* Gaussian elimination */
object GE {
  /* Coefficiant matrix */
  var M: Matrix = empty[Array[Expr]]
  var B: Array[Expr] = empty[Expr]

  val zero = Lit(GInt(0))

  type Matrix = Array[Array[Expr]]

  def init(es: List[Equation], q: List[Var]) = {
    val inputTuple = equationsToMatrix(es, q)
    M = inputTuple._1
    B = inputTuple._2
  }

  // (x,y,z) = (1,2,3) => x = 1, y = 2, z = 3
  def vectorEquations(lhs: Expr, rhs: Expr): List[Equation] = {
    var equations = List[Equation]()
    (lhs, rhs) match {
      case (ExprVector(ls1), ExprVector(ls2)) =>
        equations = (ls1 zip ls2).foldLeft(List[Equation]())((r, x) =>
          vectorEquations(x._1, x._2) ::: r) ::: equations
      case (_, _) => equations = Equation(lhs, rhs) :: equations
    }
    equations
  }
  /* Transform a set of equations into a coefficiant matrix B and rhs array.
  *  Requriment(IMPORTANT): the lhs of any input equation should be in the form of
  *  Op("+", c1*x1 :: c2*x2 :: ... cn*xn :: Nil)
  */
  def equationsToMatrix(es: List[Equation], q: List[Var]): (Matrix, Array[Expr]) = {
    // Size of Matrix N * N
    val N = es.length
    if (N != q.length)
      sys.error("Not enough equations to solve unknown variables:" + q.map(x => pprint(x.asInstanceOf[Expr])).mkString(","))

    // Initialize zero coefficiant matrix and rhs array
    var B: Array[Expr] = fill[Expr](N)(zero)
    var M: Matrix = fill[Array[Expr]](N)(fill[Expr](N)(zero))
    // Update coefficiant
    for (i <- 0 to N - 1) {
      B(i) = es(i).rhs
      for (j <- 0 to N - 1) {
        val coefs = getCoef(es(i).lhs, q(j))
        if (coefs.length > 0)
          M(i)(j) = mkPlus(coefs)
      }
    }
    // Return
    (M, B)
  }

  

  /* Main GE algorithm */
  def run(es: List[Equation], q: List[Var], hashMap: Map[Expr, Expr]): List[Equation] = {
    def findVars(e: Expr): List[Var] = findVarsE(e, hashMap)(Nil)
    def hasTrueVariable(e: Expr) = (findVars(e).toSet intersect q.toSet).size > 0
    // Break an expression into a list of basic terms, which are either constant or constant * variable
    def breakExpr(exp: Expr, vars: List[Var]): List[Expr] = exp match {
      case Lit(_) => List(exp)
      case Var(n) =>
        if (hashMap.contains(Var(n))) {
          val hashExpr = hashMap(Var(n))
          if (hasTrueVariable(hashExpr))
            breakExpr(hashExpr, vars)
          else
            List(exp)
        } else
          List(exp)
      case Dot(e1, e2) => List(exp)
      // Return x(0) as an expression 
      case Index(_, _) => List(exp)
      case Op(f, es) =>
        f.x match {
          case "+" => es.foldLeft(List[Expr]())((r, x) => r ::: breakExpr(x, vars))
          case "-" => breakExpr(es(0), vars) :::
            es.drop(1).foldLeft(List[Expr]())((r, x) => r :::
              breakExpr(mkOp("*", Lit(GInt(-1)), x), vars))
          case "*" => es match {
            case e1 :: e2 :: Nil => (hasTrueVariable(e1), hasTrueVariable(e2)) match {
              case (true, false)  => breakExpr(e1, vars).map(x => mkOp("*", x, e2))
              case (false, true)  => breakExpr(e2, vars).map(x => mkOp("*", x, e1))
              case (false, false) => List(exp)
              case (true, true)   => error("Non-linear equations")
            }

            // 1 * 2 * x case 
            case _ => error("Non-binary operator" + pprint(Op(f, es).asInstanceOf[Expr]))
          }

          case "/" => es match {
            // TODO: Maybe can do better with denominator part?
            case e1 :: e2 :: Nil => breakExpr(e1, vars).map(x => mkOp("/", x, e2))
          }
          // Other primitive functions
          case _ => List(exp)
        }
      //}
    }

    // Example: 2 * 3 * x => 6 * x
    def evalVariableTerm(exp: Expr): (Expr, Expr) = {
      implicit val exceptVars = (List.empty)
      exp match {
        case Var(n) => (Lit(GInt(1)), Var(n))
        case Op(f, es) => f.x match {
          case "*" => es match {
            case el :: er :: Nil =>
              if (findVars(el).length > 0 && findVars(er).length == 0) {
                (evalConstant(mkOp("*", er, evalVariableTerm(el)._1)),
                  evalVariableTerm(el)._2)

              } else if (findVars(er).length > 0 && findVars(el).length == 0) {
                (evalConstant(mkOp("*", el, evalVariableTerm(er)._1)),
                  evalVariableTerm(er)._2)

              } else {
                val lhs = evalVariableTerm(el)
                val rhs = evalVariableTerm(er)
                (evalConstant(mkOp("*", lhs._1, rhs._1)),
                  mkOp("*", lhs._2, rhs._2))
              }
          }
          case _ => (Lit(GInt(1)), exp)
        }
        case Index(_, _) => (Lit(GInt(1)), exp)
        case _           => error(exp.toString + " is not a basic term")
      }
    }
    // Simplify a constant expression, example 2 + 2 => 4
    def evalConstant(exp: Expr): Expr = exp match {
      case Lit(n)             => Lit(n)
      case Var(Name("pi", 0)) => Lit(GDouble(Math.PI))
      case Dot(_, _)          => exp
      case _                  => exp

    }

    /* Example 2 * (3*x) => (2*3, x)*/
    def evalTrueVarTerm(exp: Expr, trueVar: Var): (Expr, Var) = {
      exp match {
        case Var(trueVar.name) => (Lit(GInt(1)), trueVar)
        //case Index()
        case Op(Name("*", 0), e1 :: e2 :: Nil) => (e1, e2) match {
          case (Var(trueVar.name), _) => (e2, trueVar)
          case (_, Var(trueVar.name)) => (e1, trueVar)
          case _ =>
            if (findVars(e1).contains(trueVar)) {
              val runE1 = evalTrueVarTerm(e1, trueVar)
              (mkOp("*", runE1._1, e2),
                runE1._2)

            } else {
              val runE2 = evalTrueVarTerm(e2, trueVar)
              (mkOp("*", runE2._1, e1),
                runE2._2)

            }
        }
        case Op(Name("/", 0), e1 :: e2 :: Nil) =>
          if (findVars(e2).contains(trueVar))
            throw error("Variable to be solved:" + trueVar + "appears in denominator")
          else {
            val runE1 = evalTrueVarTerm(e1, trueVar)
            (mkOp("/", runE1._1, e2), runE1._2)
          }
      }

    }
    /* Divide an expr into a list of variable terms and a constant */
    def normalizeExpr(e: Expr, q: List[Var]): (Option[List[Expr]], Expr) = {
      implicit val exceptList = (List.empty)
      var finalVarTerms: Option[List[Expr]] = None
      var finalConst: Expr = Lit(GInt(0))
      val terms = breakExpr(e, q)
      // Find all the constants terms
      var constants = terms.filter(x => findVars(x).length == 0)
      var varTerms = terms.filter(x => findVars(x).length > 0)
      varTerms = varTerms.map(x => mkOp("*", evalVariableTerm(x)._1, evalVariableTerm(x)._2))
      // Terms contain true variables
      var trueVarTerms = varTerms.filter(x => findVars(x).exists(y => q.contains(y)))
      val constVarTerms = varTerms.filterNot(x => findVars(x).exists(y => q.contains(y)))
      val trueVars = trueVarTerms.map(x =>
        evalTrueVarTerm(x,
          findVars(x).find(y => q.contains(y)).get))
      constants = constants.map(evalConstant(_))
      if (constants.length > 0)
        finalConst = evalConstant(constants.drop(1).foldLeft(constants(0))((r, x) =>
          Op(Name("+", 0), r :: x :: Nil)))
      if (varTerms.length > 0)
        finalVarTerms = Some(trueVars.map(x => mkOp("*", x._1, x._2)))

      if (constVarTerms.length > 0) {
        (finalVarTerms, mkOp("+", combineConstVarTerms(constVarTerms), finalConst))
      } else
        (finalVarTerms, finalConst)
    }
    /* Nomalize an arbitray equation  */
    def normalizeEquation(e: Equation, q: List[Var]): Equation = {
      (normalizeExpr(e.lhs, q), normalizeExpr(e.rhs, q)) match {
        case ((Some(vs), cl), (None, cr)) =>
          Equation(mkPlus(vs),
            mkOp("-", cr, cl))
        case ((None, cr), (Some(vs), cl)) =>
          Equation(mkPlus(vs),
            mkOp("-", cr, cl))
        case ((Some(vs), cl), (Some(vs2), cr)) =>
          Equation(combineConstVarTerms(vs ::: vs2.map(x => mkOp("*", Lit(GInt(-1)), x))),
            mkOp("-", cr, cl))
      }
    }
    // Test whether exp has any variables from vars in it
    def hasVar(exp: Expr, vars: List[Var]): Boolean = {
      findVars(exp).exists(x => vars.contains(x))
    }

    // Main algorithm
    var ses = es.map(e => vectorEquations(e.lhs, e.rhs)).flatten
    ses = ses.map(x => normalizeEquation(x, q))
    init(ses, q)
    val N = M.length
    for (p <- 0 to N - 1) {
      // find pivot row and swap
      var max = p
      for (i <- p + 1 to N - 1) {
        if (length(M(i)(p)) > length(M(max)(p)) || isZero(M(max)(p)))
          max = i
      }
      swap(M, p, max)
      // Check singularity here
      if (isZero(M(p)(p))) {
        sys.error("Matrix is singular can't be solved")
      }
      val temp = B(p); B(p) = B(max); B(max) = temp
      // Normalization
      B(p) = mkOp("/", B(p), M(p)(p))
      M(p) = normalize(M(p)(p), M(p))
      // Pivot within M
      for (i <- p + 1 to N - 1) {
        val alpha = mkOp("/", M(i)(p), M(p)(p))
        B(i) = mkOp("-", B(i), mkOp("*", alpha, B(p)))
        for (j <- p to N - 1)
          if (j == p)
            M(i)(j) = zero
          else
            M(i)(j) = mkOp("-", M(i)(j), mkOp("*", alpha, M(p)(j)))
      }

    }
    // Back substitution
    var x = new Array[Expr](N)
    for (i <- N - 1 to 0 by -1) {
      var sum: Expr = Lit(GInt(0))
      for (j <- i + 1 to N - 1)
        sum = mkOp("+", mkOp("*", M(i)(j), x(j)), sum)

      x(i) = mkOp("-", B(i), sum)
    }

    // Output equations
    var output = List[Equation]()
    for (i <- 0 to N - 1) {
      output = Equation(q(i), x(i)) :: output
    }
    output = output.reverse
    output
  }

  /* Example: 2*x*y - 2*x*y => 0 */
  def combineConstVarTerms(varTerms: List[Expr]): Expr = {
    /* Break a const var term into coef * List(prim function) form */
    def breakConstVarTerm(varTerm: Expr): (Double, List[Expr]) = varTerm match {
      case Var(n)                                        => (1.toDouble, List(Var(n)))
      case Op(Name("*", 0), Lit(GInt(n)) :: t :: Nil)    => (n.toDouble * breakConstVarTerm(t)._1, breakConstVarTerm(t)._2)
      case Op(Name("*", 0), Lit(GDouble(n)) :: t :: Nil) => (n.toDouble * breakConstVarTerm(t)._1, breakConstVarTerm(t)._2)
      case Op(Name("*", 0), t :: Lit(GInt(n)) :: Nil)    => (n.toDouble * breakConstVarTerm(t)._1, breakConstVarTerm(t)._2)
      case Op(Name("*", 0), t :: Lit(GDouble(n)) :: Nil) => (n.toDouble * breakConstVarTerm(t)._1, breakConstVarTerm(t)._2)
      // Prim function
      case Op(f, t :: Nil)                               => (1.toDouble, List(Op(f, List(t))))
      case Op(Name("^", 0), x :: n :: Nil)               => (1.toDouble, List(Op(Name("^", 0), List(x, n))))
      case Op(Name("*", 0), t1 :: t2 :: Nil) =>
        val bt1 = breakConstVarTerm(t1)
        val bt2 = breakConstVarTerm(t2)
        (bt1._1 * bt2._1, bt1._2 ::: bt2._2)
      case Op(Name("/", 0), e1 :: e2 :: Nil) =>
        (1, List(mkOp("/", e1, e2)))
      case _ => (1, varTerm :: Nil)

    }

    var normalTerms = varTerms.map(breakConstVarTerm(_))
    var tempTerms = List[Expr]()
    var usedTerms = Set[List[Expr]]()
    for (x <- normalTerms) {
      if (!usedTerms.contains(x._2)) {
        // Find all the terms with the same prim function composition as x's
        val sameComps = normalTerms.filter(y => x._2.foldLeft(x._2.size == y._2.size)(_ && y._2.contains(_)))
        usedTerms = usedTerms ++ sameComps.map(x => x._2)
        // Combine them together
        tempTerms = mkOp("*", Lit(GDouble(sameComps.drop(1).foldLeft(sameComps(0)._1)((r, x) =>
          r.toDouble + x._1))), mkTimes(x._2)) :: tempTerms
      }
    }
    mkPlus(tempTerms)
  }
  /* Example: 3*x => 3 */
  def getCoef(e: Expr, v: Var): List[Expr] = e match {
    case Op(Name("*", 0), v1 :: v2 :: Nil) => (getCoef(v1, v), getCoef(v2, v)) match {
      case (Nil, l) => l.map(x => mkTimes(v1 :: x :: Nil))
      case (l, Nil) => l.map(x => mkTimes(v2 :: x :: Nil))
    }
    case v1: Var              => { if (v1.name == v.name) List(Lit(GInt(1))); else Nil }
    case Op(Name("+", 0), es) => es.foldLeft(List[Expr]())((r, x) => r ::: getCoef(x, v))
    //case e => Nil
    case _                    => Nil
  }

  /* A helper function to allow us to write Op("+", ...) instead of Op(Nmae("+",0),...) */
  implicit def stringToName(op: String) = Name(op, 0)

  /* Length measure for pivoting:
   * In the case of numerical stability, the largest (in absolute value)
   * is chosen, whereas for coeffi- cient growth cases, it is the smallest
   * (according to some given size metric) that is chosen.
   * Here we choose the number of symbolic terms as a length measure*/
  def length(e: Expr): Int = e match {
    case Op(_, es) => es.length
    case _         => 1
  }

  /* A superfical way for checking zero entry
   * Todo: What about l*sin(t), where it might becomes zero at certain time?*/
  def isZero(e: Expr) = e match {
    case Lit(GInt(0))      => true
    case Lit(GDouble(0.0)) => true
    case _                 => false
  }

  /* Swap row i and j*/
  def swap(m: Matrix, i: Int, j: Int) = {
    val temp = m(i)
    m(i) = m(j)
    m(j) = temp
  }
  def mkBinOp(o: String, xs: List[Expr]) = {
    xs.drop(1).foldLeft(xs(0))((r, x) => mkOp(o, r, x))
  }
  def mkOp(o: String, xs: Expr*): Expr = {
    Op(Name(o, 0), xs.toList)
  }

  def printMatrix = {
    val N = M.length
    println("********************************")
    for (i <- 0 to N - 1) {
      for (j <- 0 to N - 1)
        print(pprint(M(i)(j)) + " ")
      print(pprint(B(i)))
      println(" ")
    }
  }

  def normalize(mpp: Expr, mp: Array[Expr]): Array[Expr] = {
    var normalized: Array[Expr] = new Array[Expr](mp.length)
    for (i <- 0 to mp.length - 1) {
      normalized(i) = mkOp("/", mp(i), mpp)
    }
    normalized
  }

  /* Example: (x,2,3,y)  => ((x + 2) + 3) + y*/
  def mkPlus(es: List[Expr]): Expr = es match {
    case Nil             => Lit(GInt(0))
    case e :: Nil        => e
    case e1 :: e2 :: Nil => mkOp("+", e1, e2)
    case _ => es.drop(1).foldLeft(es(0))((r, x) =>
      mkOp("+", r, x))
  }
  def mkTimes(es: List[Expr]): Expr = es match {
    case e :: Nil => e
    case _ => es.drop(1).foldLeft(es(0))((r, x) =>
      mkOp("*", r, x))
  }
  def findVarsE(expr: Expr, bindings: Map[Expr, Expr])(implicit exceptVars: List[Var]): List[Var] = expr match {
    case Lit(n) => Nil
    case Var(n) =>
      if (exceptVars.contains(Var(n)) || n.x == "pi")
        Nil
      else {
        if (bindings.contains(Var(n)))
          findVarsE(bindings(Var(n)), bindings)
        else
          List(Var(n))
      }
    case Op(f, es)            => es.map(findVarsE(_, bindings)).flatten.toSet.toList
    case ExprVector(ls)       => ls.map(findVarsE(_, bindings)).flatten.toSet.toList
    case Index(e, idx)        => (findVarsE(e, bindings) ::: idx.map(findVarsE(_, bindings)).flatten).toSet.toList
    case Sum(e, i, col, cond) => List(e, col, cond).map(findVarsE(_, bindings)(Var(i) :: Nil)).flatten.toSet.toList
    case ExprLet(bs, e)       => findVarsE(e, bindings)(bs.map(x => Var(x._1)) ::: exceptVars)
    case Dot(_, _)            => Nil
  }

}

